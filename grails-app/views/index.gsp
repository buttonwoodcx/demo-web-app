<%@ page import="au.com.buttonwood.demo.VersionHelper" %>
<!doctype html>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Buttonwood Demo Web App</title>

  <style>
    .tab-pane {
        padding: 10px;
    }
  </style>
</head>

<body>
<content tag="nav">
</content>

<div id="content" role="main">

  <div class="container">

    <div class="row">
      <div class="col-md-12">

        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" role="tab" href="#version">Version</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" role="tab" data-toggle="tab" href="#environment">Environment</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" role="tab" data-toggle="tab" href="#properties">System Properties</a>
          </li>

        </ul>

        <g:set var="v" value="${VersionHelper.versionInfo}"/>
        <div class="tab-content">
          <div id="version" class="tab-pane fade show active" role="tabpanel">
            <table class="table table-striped table-condensed">
              <tr>
                <td>Name</td>
                <td>${v.productName}</td>
              </tr>
              <tr>
                <td>App Version</td>
                <td>${v.version}</td>
              </tr>
              <tr>
                <td>Build</td>
                <td>${v.buildNumber}</td>
              </tr>
              <tr>
                <td>Grails Env</td>
                <td>${v.grailsEnvironment}</td>
              </tr>
            </table>
          </div>

          <div id="environment" class="tab-pane fade" role="tabpanel">
            <table class="table table-striped table-condensed">
              <tr>
                <th>Name</th>
                <th>Value</th>
              </tr>
              <g:each in="${System.getenv().sort { it.key } }" var="kvp">
                <tr>
                  <td>${kvp.key}</td>
                  <td>${kvp.value}</td>
                </tr>
              </g:each>
            </table>
          </div>

          <div id="properties" class="tab-pane fade" role="tabpanel">
            <table class="table table-striped table-condensed">
              <tr>
                <th>Property</th>
                <th>Value</th>
              </tr>
              <g:each in="${System.properties?.sort { it.key }}" var="p">
                <tr>
                  <td>${p.key}</td>
                  <td>${p.value}</td>
                </tr>
              </g:each>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>

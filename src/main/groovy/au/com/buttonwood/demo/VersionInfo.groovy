package au.com.buttonwood.demo

class VersionInfo {
    private String _productName
    private String _version
    private String _buildNumber
    private String _grailsEnvironment


    VersionInfo(String productName, String version, String buildNumber, String grailsEnvironment) {
        _productName = productName
        _version = version
        _buildNumber = buildNumber
        _grailsEnvironment = grailsEnvironment
    }

    String getVersion() {
        return _version
    }

    String getBuildNumber() {
        if (_buildNumber == '@info.app.buildNumber@') {
            return "0000"
        }
        return _buildNumber
    }

    String getProductName() {
        return _productName
    }

    String getGrailsEnvironment() {
        return _grailsEnvironment
    }

    String getFullVersion() {
        return "${productName} ${_version} (build ${buildNumber}) GrailEnv: ${_grailsEnvironment}"
    }

}



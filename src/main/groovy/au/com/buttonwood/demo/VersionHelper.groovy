package au.com.buttonwood.demo

import grails.core.GrailsApplication
import grails.util.Environment

class VersionHelper {

    static final String PRODUCT_NAME = 'DemoWebApp'

    static String getVersionString() {
        return getVersionInfo()?.fullVersion
    }

    static String getShortVersionString() {
        def v = getVersionInfo()
        return "${v.version} ${v.buildNumber}"
    }

    static VersionInfo getVersionInfo() {
        def grailsApplication = BeanUtils.getBeanByName(GrailsApplication)
        def md = grailsApplication.metadata.info as Map
        def env = Environment.current?.toString()

        return new VersionInfo(PRODUCT_NAME, md.app.version as String, md.app.buildNumber as String, env)
    }

}
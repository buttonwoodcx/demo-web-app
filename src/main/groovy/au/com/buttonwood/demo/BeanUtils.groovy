package au.com.buttonwood.demo

import grails.util.Holders
import org.springframework.context.ApplicationContext

class BeanUtils {

    static <T> T getBean(Class<T> clazz) {
        ApplicationContext ctx = Holders.grailsApplication.mainContext
        T bean = (T) ctx.getBean(clazz)
        return bean
    }

    static <T> T getBean(String beanName, Class<T> clazz) {
        return Holders.applicationContext.getBean(beanName) as T
    }

    static <T> T getBeanByName(Class<T> clazz) {
        def beanName = clazz.simpleName.uncapitalize()
        return Holders.applicationContext.getBean(beanName) as T
    }

}